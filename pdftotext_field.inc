<?php

/**
 * @file
 * Common functions.
 */

 function pdftotext_field_get_file_contents($fid) {
  $contents = NULL;

  if ($file = file_load($fid)) {
    $allowed_extensions = array('pdf');

    $realpath = drupal_realpath($file->uri);
    $pathinfo = pathinfo($realpath);

    if (in_array($pathinfo['extension'], $allowed_extensions) && file_exists($realpath)) {
      $command = variable_get('pdftotext_field_helper_path');
      $command = preg_replace('/%file%/', escapeshellarg($realpath), $command);
      $results = shell_exec($command);

      if(!empty($results)) {
        $encoding = mb_detect_encoding($results);
        $contents = @mb_convert_encoding($results, 'utf-8', $encoding);
      }
    }
  }

  return $contents;
}