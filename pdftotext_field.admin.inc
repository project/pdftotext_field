<?php
/**
 * @file
 * PDF to Text Field admin file.
 *
 * Configuration form for PDF to Text Field module.
 */

/**
 * Form builder for PDF to Text Field settings.
 *
 * @see system_settings_form()
 *
 */
function pdftotext_field_settings_form($form, &$form_state) {
  $helper = variable_get('pdftotext_field_helper_path');
  
  $form['pdftotext_field'] = array(
    '#type' => 'fieldset',
    '#title' => t('Helper'),
    '#collapsible' => TRUE,
    '#collapsed' => !empty($helper)
  );
  $form['pdftotext_field']['pdftotext_field_helper_path'] = array(
    '#type' => 'textfield',
    '#title' => t('Helper'),
    '#default_value' => $helper,
    '#description' => t('Enter the path to the helper application installed on your server. "%file%" is a placeholder for the path of the attachment file and is required. Include any command-line parameters as well (for example, pdftotext requires a - after the file to be processed).'),
    '#required' => TRUE
  );

  if(!empty($helper)) {
    $content_types = array_filter(variable_get('pdftotext_field_content_types', array()));
    $node_type_names = node_type_get_names();
    $content_type_names = array();

    if(!empty($content_types)) {
      foreach($content_types as $content_type) {
        $entity_fields = field_info_instances('node', $content_type);

        if(!empty($entity_fields)) {
          $pdf_fields = $text_fields = array();

          $source = variable_get('pdftotext_field_' . $content_type . '_source', NULL);
          $target = variable_get('pdftotext_field_' . $content_type . '_target', NULL);

          $form['pdftotext_field_' . $content_type] = array(
            '#type' => 'fieldset',
            '#title' => check_plain($node_type_names[$content_type]),
            '#description' => t('The selected PDF Source field will convert PDF content into plain text and be saved into the chosen text Target field on node save. Only Media PDF upload fields are available as Source fields and only text fields are available as Target fields.'),
            '#prefix' => '<div id="RESULT_DIV_ID">', 
            '#suffix' => '</div>',
          );

          foreach($entity_fields as $entity_field) {
            $widget = $entity_field['widget'];

            if(!empty($widget) && (!array_key_exists('active', $widget) || (array_key_exists('active', $widget) && $widget['active'] == 1))) {
              if(preg_match('/^media/', $widget['type']) && preg_match('/pdf/', $entity_field['settings']['file_extensions'])) {
                $pdf_fields[$entity_field['field_name']] = $entity_field['label'];
              }

              if(preg_match('/^text/', $widget['type'])) {
                $text_fields[$entity_field['field_name']] = $entity_field['label'];
              }
            }
          }

          $form['pdftotext_field_' . $content_type]['pdftotext_field_' . $content_type . '_source'] = array(
            '#title' => t('Source'),
            '#type' => 'select',
            '#empty_option' => t('- Select -'),
            '#options' => $pdf_fields,
            '#default_value' => $source,
            '#required' => TRUE
          );

          $form['pdftotext_field_' . $content_type]['pdftotext_field_' . $content_type . '_target'] = array(
            '#title' => t('Target'),
            '#type' => 'select',
            '#empty_option' => t('- Select -'),
            '#options' => $text_fields,
            '#default_value' => $target,
            '#required' => TRUE
          );

          if(!empty($source) && !empty($target)) {
            $form['pdftotext_field_' . $content_type]['pdftotext_field_' . $content_type . '_update'] = array(
              '#title' => t('Update all %type content', array('%type' => $node_type_names[$content_type])),
              '#type' => 'checkbox',
            );
          }
        }
      }
    }

    if(!empty($node_type_names)) {
      foreach($node_type_names as $node_type => $node_type_name) {
        $entity_fields = field_info_instances('node', $node_type);

        if(!empty($entity_fields)) {
          foreach($entity_fields as $entity_field) {
            $widget = $entity_field['widget'];

            if(!empty($widget) && (!array_key_exists('active', $widget) || (array_key_exists('active', $widget) && $widget['active'] == 1))) {
              //TODO: Possibly add check for text fields?
              if(preg_match('/^media/', $widget['type']) && preg_match('/pdf/', $entity_field['settings']['file_extensions'])) {
                $content_type_names[$node_type] = $node_type_name;
              }
            }
          }
        }
      }
    }
    
    $form['content_types'] = array(
      '#type' => 'fieldset',
      '#title' => t('Content types'),
      '#description' => t('Select which content types to configure. Only content types with a Media PDF upload field are listed.'),
      '#collapsible' => TRUE,
      '#collapsed' => !empty($content_types)
    );
    $form['content_types']['pdftotext_field_content_types'] = array(
      '#type' => 'checkboxes',
      '#options' => $content_type_names,
      '#default_value' => $content_types,
    );
  }

  // Validation
  $form['#validate'][] = 'pdftotext_field_settings_form_validate';

  $form = system_settings_form($form);
  $form['#submit'][] = '_pdftotext_field_settings_form_submit';
  
  return $form;
}

function _pdftotext_settings_form_ajax_callback($form, &$form_state) {
  return '<div id="RESULT_DIV_ID">Your name is :'.'hello'.'</div>';
}


/**
 * Validation handler for the settings form.
 */
function pdftotext_field_settings_form_validate($form, &$form_state) {
  $content_types = array_filter(variable_get('pdftotext_field_content_types', array()));
  $values = $form_state['values'];

  if(!empty($content_types)) {
    foreach($content_types as $content_type) {
      if(!empty($values['pdftotext_field_'.$content_type.'_update']) && $values['pdftotext_field_'.$content_type.'_update'] == 1) {
        pdftotext_field_variables('pdftotext_field_'.$content_type.'_update', 1);
      }

      // Always set to 0
      $form_state['input']['pdftotext_field_'.$content_type.'_update'] = 0;
    }
  }
}

/**
 * Submit handler for the settings form.
 */
function _pdftotext_field_settings_form_submit($form, &$form_state) {
  $content_types = array_filter(variable_get('pdftotext_field_content_types', array()));
  $node_type_names = node_type_get_names();

  if(!empty($content_types)) {
    foreach($content_types as $content_type) {
      $update = pdftotext_field_variables('pdftotext_field_'.$content_type.'_update');

      if($update == 1) {
        $nodes = node_load_multiple(array(), array('type' => $content_type));
        $total = count($nodes);
        
        if(!empty($nodes)) {
          module_load_include('inc', 'pdftotext_field');

          foreach($nodes as $node) {
            $field_source = variable_get('pdftotext_field_' .$node->type . '_source');
            $field_target = variable_get('pdftotext_field_' .$node->type . '_target');

            if(!empty($field_source) && !empty($field_target)) {
              $fid = $node->{$field_source}[LANGUAGE_NONE][0]['fid'];
              $node->{$field_target}[LANGUAGE_NONE][0]['value'] = pdftotext_field_get_file_contents($fid);

              // Save node
              node_save($node);
            }
          }

          drupal_set_message(t("All %total %type content has been updated.", array('%total' => $total, '%type' => $node_type_names[$content_type])), 'status');
        }
      }
    }
  }
}

function pdftotext_field_variables($key, $value = NULL) {
  static $variables;

  if (isset($value)) {
    $variables[$key] = $value;
  }

  return $variables[$key];
}